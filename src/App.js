import ModalClientes from './components/modals/cliente.js';
import ModalProductos from './components/modals/producto.js';
import ModalPedidos from './components/modals/pedido.js';
import ModalReporte from './components/modals/reporte.js';
import Navbar from './components/navbar.js';

import { Card, Button } from 'react-bootstrap';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <Navbar />
      <br />
      <br />
      <br />
      <div className="container">
        <div className="row">
          <div className="col-md-6" style={{display: 'flex', justifyContent: 'center'}}>
            <Card style={{ width: '18rem', background: '#ECF3F1' }}>
              <Card.Body>
                <Card.Title>Clientes</Card.Title>
                <Card.Text>
                  En esta opcion puede Administrar los Clientes
                </Card.Text>
                <ModalClientes />
              </Card.Body>
            </Card>
          </div>
          <div className="col-md-6" style={{display: 'flex', justifyContent: 'center'}}>
            <Card style={{ width: '18rem', background: '#ECF3F1' }}>
              <Card.Body>
                <Card.Title>Productos</Card.Title>
                <Card.Text>
                  En esta opcion puede Administrar los Productos
                </Card.Text>
                <ModalProductos />
              </Card.Body>
            </Card>
          </div>
          <br />
          <br />
          <br />
          <div className="col-md-6" style={{display: 'flex', justifyContent: 'center', marginTop: '100px'}}>
            <Card style={{ width: '18rem', background: '#ECF3F1' }}>
              <Card.Body>
                <Card.Title>Pedidos</Card.Title>
                <Card.Text>
                  En esta opcion puede Crear pedidos
                </Card.Text>
                <ModalPedidos />
              </Card.Body>
            </Card>
          </div>
          <div className="col-md-6" style={{display: 'flex', justifyContent: 'center', marginTop: '100px'}}>
            <Card style={{ width: '18rem', background: '#ECF3F1' }}>
              <Card.Body>
                <Card.Title>Reporte</Card.Title>
                <Card.Text>
                  En esta opcion puede ver los pedidos realizados
                </Card.Text>
                <ModalReporte />
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
