import React, { Component } from 'react';

import { Navbar, Nav, Image } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

class NavBar extends Component {
  render() {
    return (
      <header>
        <Navbar bg="success" expand="lg">
          <Navbar.Brand>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto" style={{ margin: '0 auto' }}>
              <span className="navbar-text"  style={{color: 'white', fontWeight: 'bold', fontSize: '30px'}}>
                ADMINISTRACIÓN DE PEDIDOS
              </span>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
    );
  }
}

export default NavBar;
