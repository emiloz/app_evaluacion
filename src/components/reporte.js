import React, { Component } from 'react';

import Navbar from './navbar.js';

import axios from 'axios';
import { toast } from 'react-toastify';

import { Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

class Reporte extends Component {

  state = {
    pedidos: [],
  }

  componentDidMount = () => {
    this.getPedidos();
  }

  getPedidos = async () => {
    let { data } = await axios.get('http://localhost:1337/pedido/getPedido');
    this.setState({
      pedidos: data,
    })
  }

  render() {
    return (
      <section>
        <div className="container">
          <h1 style={{padding: '15px 0px'}}>Pedidos Realizados</h1>
        </div>
        <br />
        <Table striped bordered hover variant="success" className="container">
          <thead className="thead-dark">
            <tr>
              <th>PEDIDO</th>
              <th>NIT</th>
              <th>FECHA</th>
              <th>CODIGO PRODUCTO</th>
              <th>DESCRIPCION PRODUCTO</th>
              <th>VALOR DEL PRODUCTO</th>
              <th>TOTAL DE LA LINEA</th>
            </tr>
          </thead>
          <tbody>
            {this.state.pedidos.map(pedido =>
              <tr>
                <td>{pedido.id}</td>
                <td>{pedido.nit}</td>
                <td>{pedido.fecha}</td>
                <td>{pedido.codigo}</td>
                <td>{pedido.descripcion}</td>
                <td>{pedido.valor_producto}</td>
                <td>{pedido.valor_linea}</td>
              </tr>
            )}
          </tbody>
        </Table>
      </section>
    );
  }
}

export default Reporte;
