import React, { Component } from 'react';

import axios from 'axios';
import { toast } from 'react-toastify';

import { Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

class Pedidos extends Component {

  state = {
    clientes: [],
    productos: [],
    nit: '',
    cantidad: '',
    producto: '',
    detallePedido: [],
    idProducto: '',
  }

  componentDidMount = () => {
    this.getClientes();
    this.getProductos();
  }

  getClientes = async () => {
    let { data } = await axios.get('http://localhost:1337/clientes');
    this.setState({
      clientes: data,
    })
  }

  getProductos = async () => {
    let { data } = await axios.get('http://localhost:1337/productos');
    this.setState({
      productos: data,
    })
  }

  handleChange = (value, name) => {
    console.log('name', name, value);
    this.setState({
      ...this.state,
      [name]: value,
    });
  }

  handleChangeSelect = (value, name) => {
    if (name === "nit") {
      this.setState({
        nit: value,
      })
    }
    if (name === "producto") {
      this.setState({
        producto: value,
      })
    }
  }

  handleDetails = (e) => {
    const { cantidad, producto, nit, productos } = this.state;
    const productosJson = JSON.parse(JSON.stringify(productos));
    const productoById = productosJson.filter(p => p.id === producto.toString());

    const detalle = {
      producto: productoById[0]['id'],
      nombre: productoById[0]['descripcion'],
      cantidad: cantidad,
      valorProducto: productoById[0]['valor'],
      valorLinea: (cantidad * productoById[0]['valor']),
    };

    this.setState({
      detallePedido: [...this.state.detallePedido, detalle],
      cantidad: '',
      producto: '',
    });

  }

  onDelete = async (index, nombre, id) => {
    if (!window.confirm(`¿Esta seguro que desea eliminar el Detalle del Producto ${nombre}?`)) return false;
    const detalle = JSON.parse(JSON.stringify(this.state.detallePedido));
    const newDetalle = detalle.filter(d => d.producto !== id.toString());
    this.setState({
      detallePedido: newDetalle,
    })
  }

  handleSubmit = async (e) => {
    console.log('this.state', this.state);
    try {
      let { data } = await axios.post('http://localhost:1337/pedido/createPedido', {
        nit: this.state.nit,
        detallePedido: this.state.detallePedido,
      });
      alert('Pedido Creado con Exito');
      this.setState({
        nit: '',
        cantidad: '',
        producto: '',
        detallePedido: [],
        idProducto: '',
      })
    } catch (error) {
      console.error('error', error)
      alert('Error al crear el Cliente')
    }
  }

  render() {
    return (
      <section>
        <div className="container form-modal">
          <h1 style={{ padding: '15px 0px' }}>Creación de Pedidos</h1>
          <div className="form-group row">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Clientes</label>
              </div>
              <select
                className="custom-select"
                value={this.state.nit}
                name="nit"
                onChange={event => this.handleChangeSelect(event.target.value, event.target.name)}
              >
                <option selected>Seleccionar...</option>
                {this.state.clientes.map(cliente =>
                  <option
                    value={cliente.id}
                  >
                    {cliente.nombre}
                  </option>
                )}
              </select>
            </div>
            <div className="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Productos</label>
              </div>
              <select
                className="custom-select"
                value={this.state.producto}
                name="producto"
                onChange={event => this.handleChangeSelect(event.target.value, event.target.name)}
              >
                <option selected>Seleccionar...</option>
                {this.state.productos.map(producto =>
                  <option
                    value={producto.id}
                  >
                    {producto.descripcion}
                  </option>
                )}
              </select>
            </div>
            <div className="input-group mb-3">
              <label className="col-sm-2 col-form-label">
                Cantidad
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  className="form-control"
                  value={this.state.cantidad}
                  name="cantidad"
                  onChange={event => this.handleChange(event.target.value, event.target.name)}
                />
              </div>
            </div>
            <div class="col-sm">
              <button
                className="btn btn-primary"
                onClick={this.handleDetails}
              >
                Agregar Producto
              </button>
            </div>
          </div>
          <br />
          <br />
          <Table striped bordered hover variant="dark" className="container">
            <thead>
              <tr>
                <th>CODIGO</th>
                <th>PRODUCTO</th>
                <th>CANTIDAD</th>
                <th>VALOR PRODUCTO</th>
                <th>VALOR LINEA</th>
                <th>ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              {this.state.detallePedido.map((detalle, index) =>
                <tr>
                  <td>{detalle.producto}</td>
                  <td>{detalle.nombre}</td>
                  <td>{detalle.cantidad}</td>
                  <td>{detalle.valorProducto}</td>
                  <td>{detalle.valorLinea}</td>
                  <td>
                    <button
                      className="btn btn-danger btn-sm"
                      onClick={e => this.onDelete(index, detalle.nombre, detalle.producto)}
                    >
                      <i className="fa fa-trash" />
                    </button>
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
          <br />
          <br />
          <div class="col-sm">
            <button
              className="btn btn-primary"
              onClick={this.handleSubmit}
            >
              Guardar Pedido
            </button>
          </div>
        </div>
      </section>
    );
  }
}

export default Pedidos;
