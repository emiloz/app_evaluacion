import React, { Component } from 'react';

import Clientes from '../clientes.js';

import { Button, Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

class ModalClientes extends Component {

  state = {
    showClientes: false,
  }

  handleShowClientes = () => {
    this.setState({
      showClientes: !this.state.showClientes,
    });
  }

  handleCloseClientes = () => {
    this.setState({
      showClientes: !this.state.showClientes,
    });
  }

  render() {
    return (
      <section className="container">
        <Button variant="success" onClick={this.handleShowClientes}>
          Administrar Clientes
        </Button>
        <Modal show={this.state.showClientes} onHide={this.handleCloseClientes}>
          <Modal.Body>
            <Clientes />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseClientes}>
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal>
      </section>
    );
  }
}

export default ModalClientes;
