import React, { Component } from 'react';

import Pedidos from '../pedidos.js';

import { Button, Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

class ModalPedidos extends Component {

  state = {
    show: false,
  }

  handleShow = () => {
    this.setState({
      show: !this.state.show,
    });
  }

  handleClose = () => {
    this.setState({
      show: !this.state.show,
    });
  }

  render() {
    return (
      <section className="container">
        <Button variant="success" onClick={this.handleShow}>
          Realizar un Pedido
        </Button>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Body>
            <Pedidos />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal>
      </section>
    );
  }
}

export default ModalPedidos;
