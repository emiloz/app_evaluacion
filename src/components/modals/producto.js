import React, { Component } from 'react';

import Productos from '../productos.js';

import { Button, Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

class ModalProductos extends Component {

  state = {
    showProductos: false,
  }

  handleShowProductos = () => {
    this.setState({
      showProductos: !this.state.showProductos,
    });
  }

  handleCloseProductos = () => {
    this.setState({
      showProductos: !this.state.showProductos,
    });
  }

  render() {
    return (
      <section className="container">
        <Button variant="success" onClick={this.handleShowProductos}>
          Administrar Productos
        </Button>
        <Modal show={this.state.showProductos} onHide={this.handleCloseProductos}>
          <Modal.Body>
            <Productos />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseProductos}>
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal>
      </section>
    );
  }
}

export default ModalProductos;
