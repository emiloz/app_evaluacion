import React, { Component } from 'react';

import Reporte from '../reporte.js';

import { Button, Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

class ModalReporte extends Component {

  state = {
    showClientes: false,
  }

  handleShowClientes = () => {
    this.setState({
      showClientes: !this.state.showClientes,
    });
  }

  handleCloseClientes = () => {
    this.setState({
      showClientes: !this.state.showClientes,
    });
  }

  render() {
    return (
      <section className="container">
        <Button variant="success" onClick={this.handleShowClientes}>
          Ver Pedidos Realizados
        </Button>
        <Modal show={this.state.showClientes} onHide={this.handleCloseClientes}>
          <Modal.Body>
            <Reporte />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseClientes}>
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal>
      </section>
    );
  }
}

export default ModalReporte;
