import React, { Component } from 'react';

import Navbar from './navbar.js';

import axios from 'axios';
import { toast } from 'react-toastify';

import { Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

class Productos extends Component {

  state = {
    productos: [],
    showCreate: false,
    showUpdate: true,
    id: '',
    form: {
      id: '',
      descripcion: '',
      valor: '',
    }
  }

  componentDidMount = () => {
    this.getProductos();
  }

  getProductos = async () => {
    let { data } = await axios.get('http://localhost:1337/productos');
    this.setState({
      productos: data,
    })
  }

  handleChange = (value, name) => {
    this.setState({
      form: {
        ...this.state.form,
        [name]: value,
      }
    });
  }

  handleSubmit = async (e) => {
    try {
      let { data } = await axios.post('http://localhost:1337/productos', this.state.form);
      toast.success('Producto Creado con Exito');
      this.getProductos();
      this.setState({
        id: '',
        form: {
          id: '',
          descripcion: '',
          valor: '',
        }
      })
    } catch (error) {
      console.error('error', error)
      toast.error('Error al crear el Producto')
    }
  }

  onDelete = async (id, producto) => {
    if (!window.confirm(`¿Esta seguro que desea eliminar el Producto ${producto}?`)) return false;
    try {
      await axios.delete(`http://localhost:1337/productos/${id}`);
      this.getProductos();
      toast.success('Producto Eliminado con Exito');
    } catch (error) {
      console.error('error', error)
      toast.error('Error al eliminar el Producto')
    }
  }

  onUpdate = async (producto) => {
    this.setState({
      showCreate: true,
      showUpdate: false,
    });
    this.setState({
      id: producto.id,
      form: producto,
    });
  }

  onCancelUpdate = () => {
    this.setState({
      showCreate: false,
      showUpdate: true,
      id: '',
      form: {
        id: '',
        descripcion: '',
        valor: '',
      }
    })
  }

  handleUpdate = async () => {
    try {
      const { data } = await axios.patch(`http://localhost:1337/productos/${this.state.id}`, this.state.form);
      console.log('handleUpdate', data);
      this.getProductos();
      this.setState({
        showCreate: false,
        showUpdate: true,
        id: '',
        form: {
          id: '',
          descripcion: '',
          valor: '',
        }
      })
      alert('Producto Actualizado con Exito');
    } catch (error) {
      console.error('error', error)
      alert('Error al actualizar el Producto')
    }
  }

  render() {
    return (
      <section>
        <div className="container form-modal">
          <h1 style={{padding: '15px 0px'}}>Catálogo de Productos</h1>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label">
              Codigo
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                value={this.state.form.id}
                name="id"
                onChange={event => this.handleChange(event.target.value, event.target.name)}
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label">
              Descripcion
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                value={this.state.form.descripcion}
                name="descripcion"
                onChange={event => this.handleChange(event.target.value, event.target.name)}
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label">
              Valor
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                value={this.state.form.valor}
                name="valor"
                onChange={event => this.handleChange(event.target.value, event.target.name)}
              />
            </div>
          </div>
          <br />
          <div className="container">
            <div className="row">
              <div className="col-sm">
                <button
                  className="btn btn-primary"
                  onClick={this.handleSubmit}
                  disabled={this.state.showCreate}
                >
                  Crear Producto
                </button>
              </div>
              <div className="col-sm">
                <button
                  className="btn btn-primary"
                  onClick={this.handleUpdate}
                  disabled={this.state.showUpdate}
                >
                  Actualizar Producto
                </button>
                {!this.state.showUpdate && <button
                  style={{ marginLeft: '5px'}}
                  className="btn btn-danger"
                  onClick={this.onCancelUpdate}
                  disabled={this.state.showUpdate}
                >
                  Cancelar Actualizacion
                </button>}
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
        <Table striped bordered hover variant="dark" className="container">
          <thead>
            <tr>
              <th>CODIGO</th>
              <th>DESCRIPCION</th>
              <th>VALOR</th>
              <th>ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            {this.state.productos.map(producto =>
              <tr>
                <td>{producto.id}</td>
                <td>{producto.descripcion}</td>
                <td>{producto.valor}</td>
                <td>
                  <button
                    className="btn btn-danger btn-sm"
                    onClick={e => this.onDelete(producto.id, producto.descripcion)}
                  >
                    <i className="fa fa-trash" />
                  </button>
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={e => this.onUpdate(producto)}
                  >
                    <i className="fa fa-pencil" />
                  </button>
                </td>
              </tr>
            )}
          </tbody>
        </Table>
      </section>
    );
  }
}

export default Productos;
