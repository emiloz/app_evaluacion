import React, { Component } from 'react';

import Navbar from './navbar.js';

import axios from 'axios';
import { toast } from 'react-toastify';

import { Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

class Clientes extends Component {

  state = {
    clientes: [],
    showCreate: false,
    showUpdate: true,
    id: '',
    form: {
      id: '',
      nombre: '',
      direcccion: '',
      categoria: '',
    }
  }

  componentDidMount = () => {
    this.getClientes();
  }

  getClientes = async () => {
    let { data } = await axios.get('http://localhost:1337/clientes');
    this.setState({
      clientes: data,
    })
  }

  handleChange = (value, name) => {
    this.setState({
      form: {
        ...this.state.form,
        [name]: value,
      }
    });
  }

  handleSubmit = async (e) => {
    try {
      let { data } = await axios.post('http://localhost:1337/clientes', this.state.form);
      toast.success('Cliente Creado con Exito');
      this.getClientes();
      this.setState({
        id: '',
        form: {
          id: '',
          nombre: '',
          direcccion: '',
          categoria: '',
        }
      })
      alert('Cliente creado con Exito');
    } catch (error) {
      console.error('error', error)
      alert('Error al crear el Cliente')
    }
  }

  onDelete = async (id, cliente) => {
    if (!window.confirm(`¿Esta seguro que desea eliminar el Cliente ${cliente}?`)) return false;
    try {
      await axios.delete(`http://localhost:1337/clientes/${id}`);
      this.getClientes();
      toast.success('Cliente Eliminado con Exito');
    } catch (error) {
      console.error('error', error)
      toast.error('Error al eliminar el Cliente')
    }
  }

  onUpdate = async (cliente) => {
    this.setState({
      showCreate: true,
      showUpdate: false,
    });
    this.setState({
      id: cliente.id,
      form: cliente,
    });
  }

  onCancelUpdate = () => {
    this.setState({
      showCreate: false,
      showUpdate: true,
      id: '',
      form: {
        id: '',
        nombre: '',
        direcccion: '',
        categoria: '',
      }
    })
  }

  handleUpdate = async () => {
    try {
      const { data } = await axios.patch(`http://localhost:1337/clientes/${this.state.id}`, this.state.form);
      console.log('handleUpdate', data);
      this.getClientes();
      this.setState({
        showCreate: false,
        showUpdate: true,
        id: '',
        form: {
          id: '',
          nombre: '',
          direcccion: '',
          categoria: '',
        }
      })
      toast.success('Cliente Actualizado con Exito');
    } catch (error) {
      console.error('error', error)
      toast.error('Error al actualizar el Cliente')
    }
  }

  render() {
    return (
      <section>
        <div className="container form-modal">
          <h1 style={{padding: '15px 0px'}}>Catálogo de Clientes</h1>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label">
              NIT
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                value={this.state.form.id}
                name="id"
                onChange={event => this.handleChange(event.target.value, event.target.name)}
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label">
              Nombre Completo
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                value={this.state.form.nombre}
                name="nombre"
                onChange={event => this.handleChange(event.target.value, event.target.name)}
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label">
              Dirección
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                value={this.state.form.direcccion}
                name="direcccion"
                onChange={event => this.handleChange(event.target.value, event.target.name)}
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label">
              Categoría
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                value={this.state.form.categoria}
                name="categoria"
                onChange={event => this.handleChange(event.target.value, event.target.name)}
              />
            </div>
          </div>
          <br />
          <div class="container">
            <div class="row">
              <div class="col-sm">
                <button
                  className="btn btn-primary"
                  onClick={this.handleSubmit}
                  disabled={this.state.showCreate}
                >
                  Crear Cliente
                </button>
              </div>
              <div class="col-sm">
                <button
                  className="btn btn-primary"
                  onClick={this.handleUpdate}
                  disabled={this.state.showUpdate}
                >
                  Actualizar Cliente
                </button>
                {!this.state.showUpdate && <button
                  style={{ marginLeft: '5px'}}
                  className="btn btn-danger"
                  onClick={this.onCancelUpdate}
                  disabled={this.state.showUpdate}
                >
                  Cancelar Actualizacion
                </button>}
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
        <Table striped bordered hover variant="dark" className="container">
          <thead>
            <tr>
              <th>NIT</th>
              <th>NOMBRE COMPLETO</th>
              <th>DIRECCION</th>
              <th>CATEGORIA</th>
              <th>ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            {this.state.clientes.map(cliente =>
              <tr>
                <td>{cliente.id}</td>
                <td>{cliente.nombre}</td>
                <td>{cliente.direcccion}</td>
                <td>{cliente.categoria}</td>
                <td>
                  <button
                    className="btn btn-danger btn-sm"
                    onClick={e => this.onDelete(cliente.id, cliente.nombre)}
                  >
                    <i className="fa fa-trash" />
                  </button>
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={e => this.onUpdate(cliente)}
                  >
                    <i className="fa fa-pencil" />
                  </button>
                </td>
              </tr>
            )}
          </tbody>
        </Table>
      </section>
    );
  }
}

export default Clientes;
